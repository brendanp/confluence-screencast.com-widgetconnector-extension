package net.artemissoftware;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import org.apache.commons.lang.StringEscapeUtils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * this WidgetConnector extension is developed somewhat from these instructions:
 * https://studio.plugins.atlassian.com/wiki/display/WC/Extending+the+Widget+Connector
 * <p/>
 * Basically this class grabs the entire page at screencast.com and then looks for <object>...</object> tags which surround the
 * content we can embed in a Confluence page to embed a screen cast video
 * <p/>
 * This class implements the interface which the WidgetConnector looks through in it's list of components
 * when determining what to do with a url passed to it by leveraging osgi.  Surprisingly this all works :)
 */
public class ScreencastRenderer implements WidgetRenderer {
    //    private static final String MATCH_URL = "screenyoutube.com";
    private static final String MATCH_URL = "screencast.com";

    //    private static final Pattern PATTERN = Pattern.compile("t/([^&]+)");
    private static final Pattern PATTERN = Pattern.compile("http://www.screencast.com/([^&]+)");
    private VelocityRenderService velocityRenderService;

    public ScreencastRenderer(VelocityRenderService velocityRenderService) {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url) {
        Matcher m = PATTERN.matcher(url);

        String videoId = "";

        if (m.find()) {
            videoId = m.group(1);
        }

        return videoId;
    }

    public boolean matches(String url) {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params) {

        if (url.startsWith("http://content")) {
            StringBuilder sb = new StringBuilder();
            sb.append("<h5>embed object id not found on this screencast page</h5>\n");
            sb.append("==========================\n<br/>");
            sb.append("Please make sure you are using the URL from the browser's url bar and not info in the obj id tag.<br/>\n");
//            sb.append(webStr);
            sb.append("==========================\n<br/>");
            return sb.toString();

        }

        // retrieve contents from URL
        String webStr = Util.getWebPage(url);
        String parts[] = webStr.split("(&lt;object|object&gt;)");


        // couldn't find escaped object tag to embed, try just what the page itself is displaying
        if (parts.length < 3) {
            parts = webStr.split("(<object|object>)");
        }

        // if the object tag still wasn't foud post an error
        if (parts.length < 3) {

            StringBuilder sb = new StringBuilder();
            sb.append("<h5>embed object id not found on this screencast page</h5>\n");
            sb.append("==========================\n<br/>");
            sb.append("Please make sure you are using the URL from the browser's url bar and not info in the obj id tag.<br/>\n");
//            sb.append(webStr);
            sb.append("==========================\n<br/>");
            return sb.toString();

        }
        String objectEmbedPart = StringEscapeUtils.unescapeHtml(parts[1]);

        // add back the bits that were stripped while splitting into parts
        objectEmbedPart = "<object" + objectEmbedPart + "object>";

        // send back obj tag contents
        return objectEmbedPart;
    }

}
